var controllerModule = angular.module('starter.controllers', []);

controllerModule.controller('authInterviewerCtrl', function($scope,$state,$cordovaFile,$ionicLoading) {


    $scope.authUserGo = function (login, city, adress, sfdc) {

        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Отправка анкет на сервер!'
        });
        if(login == undefined || city == undefined || adress == undefined || sfdc == undefined){
            //alert('Заполните все поля');
            $ionicLoading.hide();
            navigator.notification.alert(
                'Необходимо корректно заполнить все поля.',
                function(){},
                'Авторизация',
                'Закрыть'
            );
        }else{
            var id = checkInterviewer(login.toLowerCase());
            if(id > 0) {
                $ionicLoading.hide();
                $scope.nextPage(id, city, adress, sfdc);
            }else{
                $ionicLoading.hide();
                navigator.notification.alert(
                    'Некоректный логин, доступ закрыт.',
                    function(){},
                    'Авторизация',
                    'Закрыть'
                );
            }
        }
    }


    $scope.nextPage = function(loginId, city, adress, sfdc){

        window.localStorage.setItem('rInCity', city);
        window.localStorage.setItem('rInUserId', loginId);
        window.localStorage.setItem('rInTT', adress);
        window.localStorage.setItem('rInSfdc', sfdc);

        $state.go('startWorkPage');
    };
});


controllerModule.controller('startWorkPageCtrl', function($scope,$state,$cordovaFile,$ionicLoading, $ionicPlatform,$http,$cordovaNetwork) {

    if(window.localStorage.getItem('rInCity') == null || window.localStorage.getItem('rInUserId') == null ||
        window.localStorage.getItem('rInTT') == null || window.localStorage.getItem('rInSfdc') == null ){
        $state.go('authPage');
    }

    $scope.userId = window.localStorage.getItem('rInUserId');

    $scope.keyWord = window.localStorage.getItem('rInSfdc');

    window.localStorage.removeItem('rExCode');
    window.localStorage.removeItem('rDateCreate');
    window.localStorage.removeItem('rUserMarkaSig');
    window.localStorage.removeItem('rUserStrongSig');
    window.localStorage.removeItem('rUserName');
    window.localStorage.removeItem('rUserLastName');
    window.localStorage.removeItem('rUserDate');
    window.localStorage.removeItem('rUserEmail');
    window.localStorage.removeItem('rUserPhone');
    window.localStorage.removeItem('rUserSign');
    window.localStorage.removeItem('rUserSignDate');
    window.localStorage.removeItem('rUserQ1');
    window.localStorage.removeItem('rUserQ2');
    window.localStorage.removeItem('rUserQ3');
    window.localStorage.removeItem('rUserQ4');
    window.localStorage.removeItem('');

    $scope.createForm = function(){

        var dateForm = new Date().getTime();
        var exCode = window.localStorage.getItem('rInUserId') + '_' + dateForm;

        window.localStorage.setItem('rExCode', exCode);
        window.localStorage.setItem('rDateCreate', dateForm);


        $state.go('regInterviewerPage');

    };


    $scope.uploadForm = function () {

        if($cordovaNetwork.getNetwork() != 'none' && $cordovaNetwork.isOnline()){
            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Отправка анкет на сервер!'
            });

            $cordovaFile.readAsText(cordova.file.dataDirectory, "forms.txt")
                .then(function (success) {
                    var strData = success;

                    if(strData.length > 20) {

                        strData = strData.substring(0, strData.length - 2);
                        var arDataForms = strData.split('||');
                        console.log(arDataForms.length);

                        $http.post('http://maxim.global-man.ru/local/action/api.php', {method: 'save', data: strData}, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function (result) {
                            console.log(JSON.stringify(result));
                            $ionicLoading.hide();
                            if (result.success) {
                                navigator.notification.alert(
                                    'Данные на сервере сохранены!',
                                    function(){},
                                    'Отправка анкет',
                                    'Закрыть'
                                );
                                //alert('Данные на сервере сохранены!');
                            } else {
                                navigator.notification.alert(
                                    'Ошибка сохранения данных на сервере!',
                                    function(){},
                                    'Отправка анкет',
                                    'Закрыть'
                                );
                                //alert('Ошибка сохранения данных на сервере!');
                            }
                        }).error(function(data, status, headers, config) {
                            $ionicLoading.hide();
                            if(status != ''){
                                navigator.notification.alert(
                                    'Ошибка сохранения данных на сервере! Пожалуйста сделайте принтскрин этого сообщения и передайте его супервайзеру.\n\nКод ошибки: '+status+'.\n\nДополнительная информация: '+headers+'.',
                                    function(){},
                                    'Отправка анкет',
                                    'Закрыть'
                                );
                            } else {
                                navigator.notification.alert(
                                    'Ошибка сохранения данных на сервере! Пожалуйста сделайте принтскрин этого сообщения и передайте его супервайзеру.\n\nКод ошибки: '+status+'.\n\nДополнительная информация: '+headers+'.',
                                    function(){},
                                    'Отправка анкет',
                                    'Закрыть'
                                );
                            }
                        });

                    } else {
                        $ionicLoading.hide();
                        navigator.notification.alert(
                            'На устройстве нет анкет для отправки на сервер.',
                            function(){},
                            'Отправка анкет',
                            'Закрыть'
                        );
                    }
                }, function (error) {
                    $ionicLoading.hide();
                    alert(err);
                });

        } else {
            navigator.notification.alert(
                'У вас отсутсвует интерент соединение!',
                function(){},
                'Отправка анкет',
                'Закрыть'
            );
        }
    }


    $scope.exitApp = function () {

        window.localStorage.removeItem('rInCity');
        window.localStorage.removeItem('rInUserId');
        window.localStorage.removeItem('rInTT');
        window.localStorage.removeItem('rInSfdc');

        document.location.href = 'index.html';
    }



    $scope.copyFiletoCashe = function () {
        var key = new Date().getTime();
        $cordovaFile.copyFile(cordova.file.dataDirectory, "forms.txt", cordova.file.externalDataDirectory, "form_"+key+".txt")
            .then(function (success) {
                navigator.notification.alert(
                    'Файл успешно скопирован! Название файла forms'+key+'.txt.',
                    function(){},
                    'Копирование файла',
                    'Закрыть'
                );
            }, function (error) {
                navigator.notification.alert(
                    'Ошибка при копирование файла.',
                    function(){},
                    'Копирование файла',
                    'Закрыть'
                );
            });
    }

    $scope.uploadFormAll = function () {

        if($cordovaNetwork.getNetwork() != 'none' && $cordovaNetwork.isOnline()){
            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Отправка анкет на сервер!'
            });


            $cordovaFile.readAsText(cordova.file.dataDirectory, "forms.txt")
                .then(function (success) {
                    var strData = success;
                    strData = strData + strData + strData + strData + strData + strData;


                    if(strData.length > 20) {

                        strData = strData.substring(0, strData.length - 2);
                        var arDataForms = strData.split('||');
                        var key = new Date().getTime();
                        console.log(arDataForms.length);

                        $http.post('http://maxim.global-man.ru/local/action/api_save_file.php', {method: 'save', data: strData , key: key}, {
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function (result) {
                            $ionicLoading.hide();
                            if (result) {
                                navigator.notification.alert(
                                    'Данные на сервере сохранены! Клуч к файлу '+key+'.',
                                    function(){},
                                    'Отправка анкет',
                                    'Закрыть'
                                );
                                //alert('Данные на сервере сохранены!');
                            } else {
                                navigator.notification.alert(
                                    'Ошибка сохранения данных на сервере!',
                                    function(){},
                                    'Отправка анкет',
                                    'Закрыть'
                                );
                                //alert('Ошибка сохранения данных на сервере!');
                            }
                        }).error(function(data, status, headers, config) {
                            $ionicLoading.hide();
                            if(status != ''){
                                navigator.notification.alert(
                                    'Ошибка сохранения данных на сервере! Пожалуйста сделайте принтскрин этого сообщения и передайте его супервайзеру.\n\nКод ошибки: '+status+'.\n\nДополнительная информация: '+headers+'.',
                                    function(){},
                                    'Отправка анкет',
                                    'Закрыть'
                                );
                            } else {
                                navigator.notification.alert(
                                    'Ошибка сохранения данных на сервере! Пожалуйста сделайте принтскрин этого сообщения и передайте его супервайзеру.\n\nКод ошибки: '+status+'.\n\nДополнительная информация: '+headers+'.',
                                    function(){},
                                    'Отправка анкет',
                                    'Закрыть'
                                );
                            }
                        });

                    } else {
                        $ionicLoading.hide();
                        navigator.notification.alert(
                            'На устройстве нет анкет для отправки на сервер.',
                            function(){},
                            'Отправка анкет',
                            'Закрыть'
                        );
                    }
                }, function (error) {
                    $ionicLoading.hide();
                    alert(err);
                });

        } else {
            navigator.notification.alert(
                'У вас отсутсвует интерент соединение!',
                function(){},
                'Отправка анкет',
                'Закрыть'
            );
        }
    }

});


controllerModule.controller('signPageCtrl', function($scope,$state) {

    var now = new Date();
    $scope.dateNow = now.getDate()+'.'+(now.getMonth()+1)+'.'+now.getFullYear()+' '+now.getHours()+':'+now.getMinutes();

    var CanvasDrawr = function(options) {
        var canvas = document.getElementById(options.id),
            ctxt = canvas.getContext("2d");
        ctxt.clearRect(0, 0, canvas.width, canvas.height);
        ctxt.lineWidth = options.size;
        ctxt.lineCap = "round";
        ctxt.pX = undefined;
        ctxt.pY = undefined;
        var lines = [,,];
        var self = {
            init: function() {

                canvas.addEventListener('touchstart', self.preDraw, false);
                canvas.addEventListener('touchmove', self.draw, false);
                canvas.addEventListener('touchend', self.save, false);
            },
            preDraw: function(event) {
                $.each(event.touches, function(i, touch) {
                    var id = touch.identifier;
                    lines[id] = { x     : this.pageX - 492,
                        y     : this.pageY - 455,
                        color : options.color
                    };
                });
                event.preventDefault();
            },
            draw: function(event) {
                var e = event, hmm = {};
                $.each(event.touches, function(i, touch) {
                    var id = touch.identifier;
                    var moveX = this.pageX - 492 - lines[id].x,
                        moveY = this.pageY - 455 - lines[id].y;
                    var ret = self.move(id, moveX, moveY);
                    lines[id].x = ret.x;
                    lines[id].y = ret.y;
                });
                event.preventDefault();
            },
            move: function(i, changeX, changeY) {
                ctxt.strokeStyle = lines[i].color;
                ctxt.beginPath();
                ctxt.moveTo(lines[i].x, lines[i].y);
                ctxt.lineTo(lines[i].x + changeX, lines[i].y + changeY);
                ctxt.stroke();
                ctxt.closePath();
                return { x: lines[i].x + changeX, y: lines[i].y + changeY };
            },
            save: function () {
                var pngUrl = canvas.toDataURL();
                window.localStorage.setItem('rUserSign', pngUrl);

            }
        };
        return self.init();
    };
    var canvas_drawing = new CanvasDrawr({id:"canvas", size: 2, color: "red" });

    $scope.clearSign = function(){
        var canvas = document.getElementById("canvas"),
            ctxt = canvas.getContext("2d");
        ctxt.clearRect(0, 0, canvas.width, canvas.height);
    };

    $scope.nextPage = function(){

        if(!!window.localStorage.getItem('rUserSign')) {
            var dateNow = new Date().getTime();

            window.localStorage.setItem('rUserSignDate', dateNow);

            $state.go('oneQPage');
        }else{

            navigator.notification.alert(
                'Вам необходимо оставить электронную подпись.',
                function(){},
                'Электронная подпись',
                'Исправить'
            );

        }
    };

});

controllerModule.controller('regInterviewerPageCtrl', function($scope,$state,$cordovaGeolocation) {

    var todayMinBorn = new Date();
    todayMinBorn.setDate(todayMinBorn.getDate()-1);
    todayMinBorn.setYear(todayMinBorn.getFullYear()-35);

    var todayMinBornMonth = todayMinBorn.getMonth()+1;
    var todayMinBornDate = todayMinBorn.getDate();
    if (todayMinBornMonth < 10) todayMinBornMonth = '0' + todayMinBornMonth;
    if (todayMinBornDate <10) todayMinBornDate = '0' + todayMinBornDate;

    $('.interviewer-page-i5').attr('max',todayMinBorn.getFullYear()+'-'+todayMinBornMonth+'-'+todayMinBornDate);

    $scope.nextPage = function(formMarkaSig,formStrongSig,formUserName,formUserLastName,formUserDate,formUserEmail,formrUserPhone){

        var userDateForm;
        if(!!formUserDate) {
            var userDate = new Date(formUserDate);
            var userDateTimestamp = userDate.getTime();
            userDateForm = userDate.getDate() + '.' + (userDate.getMonth() + 1) + '.' + userDate.getFullYear();
        }

        if(!!formMarkaSig && !!formStrongSig && !!formUserName && !!formUserLastName && !!formUserDate && !!formUserEmail && !!formrUserPhone ) {

            var errArr = [];

            if (checkEmail(formUserEmail) === false) {
                errArr.push('-Введите корректный email.');
            }

            if (checkTextCiril(formUserName) === false) {
                errArr.push('-Введите корректное имя.');
            }

            if (checkTextCiril(formUserLastName) === false) {
                errArr.push('-Введите корректную фамилию.');
            }

            if (parseInt(formrUserPhone.substr(0,1)) != 7) {
                errArr.push('-Номер телефона должен начинаться с 7.');
            }

            if (formrUserPhone.length != 11) {
                errArr.push('-Номер телефона должен состоять из 11 символов.');
            }

            if (parseInt(getAge(userDateForm)) <= 18) {
                errArr.push('-Вам должно быть больше 18 лет.');
            }


            if (errArr.length > 0){
                var messageErr = '';
                for (var i = errArr.length - 1; i >= 0; i--) {
                    messageErr = messageErr +'\n\r'+ errArr[i];
                };

                navigator.notification.alert(
                    messageErr,
                    function(){},
                    'Регистрация',
                    'Исправить'
                );
                //alert(messageErr);

            } else{

                var posOptions = {timeout: 10000, enableHighAccuracy: false};
                $cordovaGeolocation
                    .getCurrentPosition(posOptions)
                    .then(function (position) {
                        window.localStorage.setItem('rUserLat', position.coords.latitude);
                        window.localStorage.setItem('rUserLong', position.coords.longitude);
                    }, function(err) {
                        // error
                    });

                window.localStorage.setItem('rUserMarkaSig', formMarkaSig);
                window.localStorage.setItem('rUserStrongSig', formStrongSig);
                window.localStorage.setItem('rUserName', formUserName);
                window.localStorage.setItem('rUserLastName', formUserLastName);
                window.localStorage.setItem('rUserDate', userDateTimestamp);
                window.localStorage.setItem('rUserEmail', formUserEmail);
                window.localStorage.setItem('rUserPhone', formrUserPhone);

                if (window.localStorage.getItem('rUserLastName') == formUserLastName) {
                    $state.go('signPage');
                }
            }
        }else{
            navigator.notification.alert(
                'Необходимо корректно заполнить все поля.',
                function(){},
                'Регистрация',
                'Закрыть'
            );
            //alert('Заполните все поля');
        }
    };

});

controllerModule.controller('oneQPageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading) {

    $scope.userLastName =  window.localStorage.getItem('rUserLastName');
    $scope.userName = window.localStorage.getItem('rUserName');


    $scope.nextPage = function(formUserQ1){
        window.localStorage.setItem('rUserQ1', formUserQ1);
        $state.go('twoQPage');
    };


    $scope.stopPage = function(){
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты на устройство!'
        });
        var dataSave = {};
        var dateFormEnd = new Date().getTime();

        dataSave.rExCode = window.localStorage.getItem('rExCode');
        dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
        dataSave.rInCity = window.localStorage.getItem('rInCity');
        dataSave.rInUserId = window.localStorage.getItem('rInUserId');
        dataSave.rInTT = window.localStorage.getItem('rInTT');
        dataSave.rInSfdc = window.localStorage.getItem('rInSfdc');
        dataSave.rUserLat = window.localStorage.getItem('rUserLat');
        dataSave.rUserLong = window.localStorage.getItem('rUserLong');
        dataSave.rUserSign = window.localStorage.getItem('rUserSign');
        dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
        dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
        dataSave.rUserStrongSig = window.localStorage.getItem('rUserStrongSig');
        dataSave.rUserName = window.localStorage.getItem('rUserName');
        dataSave.rUserLastName = window.localStorage.getItem('rUserLastName');
        dataSave.rUserDate = window.localStorage.getItem('rUserDate');
        dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
        dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
        dataSave.rUserQ1 = '-';
        dataSave.rUserQ2 = '-';
        dataSave.rUserQ3 = '-';
        dataSave.rUserQ4 = '-';
        dataSave.rUserQ5 = '-';
        dataSave.rCurtForm = 1;
        dataSave.rUserWinner = 'NO';
        dataSave.rDateEnd = dateFormEnd;

        var strSave = JSON.stringify(dataSave) + '||';

        $cordovaFile.writeExistingFile(cordova.file.dataDirectory, "forms.txt", strSave)
            .then(function (success) {
                //console.log(strSave);
                $ionicLoading.hide();
                document.location.href = 'index.html';
            }, function (error) {
                $ionicLoading.hide();
                alert(error);
            });
    };


});

controllerModule.controller('twoQPageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading) {

    $scope.userLastName =  window.localStorage.getItem('rUserLastName');
    $scope.userName = window.localStorage.getItem('rUserName');

    $scope.nextPage = function(formUserQ2){
        window.localStorage.setItem('rUserQ2', formUserQ2);
        if(formUserQ2 == 'Да') {
            $state.go('threeQPage');
        }else{
            window.localStorage.setItem('rUserQ3', 'Пропуск вопроса');
            $state.go('fourQPage');
        }
    };


    $scope.stopPage = function(){
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты на устройство!'
        });
        var dataSave = {};
        var dateFormEnd = new Date().getTime();

        dataSave.rExCode = window.localStorage.getItem('rExCode');
        dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
        dataSave.rInCity = window.localStorage.getItem('rInCity');
        dataSave.rInUserId = window.localStorage.getItem('rInUserId');
        dataSave.rInTT = window.localStorage.getItem('rInTT');
        dataSave.rInSfdc = window.localStorage.getItem('rInSfdc');
        dataSave.rUserLat = window.localStorage.getItem('rUserLat');
        dataSave.rUserLong = window.localStorage.getItem('rUserLong');
        dataSave.rUserSign = window.localStorage.getItem('rUserSign');
        dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
        dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
        dataSave.rUserStrongSig = window.localStorage.getItem('rUserStrongSig');
        dataSave.rUserName = window.localStorage.getItem('rUserName');
        dataSave.rUserLastName = window.localStorage.getItem('rUserLastName');
        dataSave.rUserDate = window.localStorage.getItem('rUserDate');
        dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
        dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
        dataSave.rUserQ1 = window.localStorage.getItem('rUserQ1');
        dataSave.rUserQ2 = '-';
        dataSave.rUserQ3 = '-';
        dataSave.rUserQ4 = '-';
        dataSave.rUserQ5 = '-';
        dataSave.rCurtForm = 1;
        dataSave.rUserWinner = 'NO';
        dataSave.rDateEnd = dateFormEnd;

        var strSave = JSON.stringify(dataSave) + '||';

        $cordovaFile.writeExistingFile(cordova.file.dataDirectory, "forms.txt", strSave)
            .then(function (success) {
                //console.log(strSave);
                $ionicLoading.hide();
                document.location.href = 'index.html';
            }, function (error) {
                $ionicLoading.hide();
                alert(error);
            });
    };

});


controllerModule.controller('threeQPageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading) {

    $scope.userLastName =  window.localStorage.getItem('rUserLastName');
    $scope.userName = window.localStorage.getItem('rUserName');

    $scope.nextPage = function(formUserQ3){
        window.localStorage.setItem('rUserQ3', formUserQ3);
        $state.go('fourQPage');
    };

    $scope.stopPage = function(){
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты на устройство!'
        });
        var dataSave = {};
        var dateFormEnd = new Date().getTime();

        dataSave.rExCode = window.localStorage.getItem('rExCode');
        dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
        dataSave.rInCity = window.localStorage.getItem('rInCity');
        dataSave.rInUserId = window.localStorage.getItem('rInUserId');
        dataSave.rInTT = window.localStorage.getItem('rInTT');
        dataSave.rInSfdc = window.localStorage.getItem('rInSfdc');
        dataSave.rUserLat = window.localStorage.getItem('rUserLat');
        dataSave.rUserLong = window.localStorage.getItem('rUserLong');
        dataSave.rUserSign = window.localStorage.getItem('rUserSign');
        dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
        dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
        dataSave.rUserStrongSig = window.localStorage.getItem('rUserStrongSig');
        dataSave.rUserName = window.localStorage.getItem('rUserName');
        dataSave.rUserLastName = window.localStorage.getItem('rUserLastName');
        dataSave.rUserDate = window.localStorage.getItem('rUserDate');
        dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
        dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
        dataSave.rUserQ1 = window.localStorage.getItem('rUserQ1');
        dataSave.rUserQ2 = window.localStorage.getItem('rUserQ2');
        dataSave.rUserQ3 = window.localStorage.getItem('rUserQ3');
        dataSave.rUserQ4 = '-';
        dataSave.rUserQ5 = '-';
        dataSave.rCurtForm = 1;
        dataSave.rUserWinner = 'NO';
        dataSave.rDateEnd = dateFormEnd;

        var strSave = JSON.stringify(dataSave) + '||';

        $cordovaFile.writeExistingFile(cordova.file.dataDirectory, "forms.txt", strSave)
            .then(function (success) {
                //console.log(strSave);
                $ionicLoading.hide();
                document.location.href = 'index.html';
            }, function (error) {
                $ionicLoading.hide();
                alert(error);
            });
    };

});


controllerModule.controller('fourQPageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading) {

    $scope.userLastName =  window.localStorage.getItem('rUserLastName');
    $scope.userName = window.localStorage.getItem('rUserName');

    $scope.nextPage = function(formUserQ4){
        window.localStorage.setItem('rUserQ4', formUserQ4);
        $state.go('informPage');
    };


    $scope.stopPage = function(){
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты на устройство!'
        });
        var dataSave = {};
        var dateFormEnd = new Date().getTime();

        dataSave.rExCode = window.localStorage.getItem('rExCode');
        dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
        dataSave.rInCity = window.localStorage.getItem('rInCity');
        dataSave.rInUserId = window.localStorage.getItem('rInUserId');
        dataSave.rInTT = window.localStorage.getItem('rInTT');
        dataSave.rInSfdc = window.localStorage.getItem('rInSfdc');
        dataSave.rUserLat = window.localStorage.getItem('rUserLat');
        dataSave.rUserLong = window.localStorage.getItem('rUserLong');
        dataSave.rUserSign = window.localStorage.getItem('rUserSign');
        dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
        dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
        dataSave.rUserStrongSig = window.localStorage.getItem('rUserStrongSig');
        dataSave.rUserName = window.localStorage.getItem('rUserName');
        dataSave.rUserLastName = window.localStorage.getItem('rUserLastName');
        dataSave.rUserDate = window.localStorage.getItem('rUserDate');
        dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
        dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
        dataSave.rUserQ1 = window.localStorage.getItem('rUserQ1');
        dataSave.rUserQ2 = window.localStorage.getItem('rUserQ2');
        dataSave.rUserQ3 = '-';
        dataSave.rUserQ4 = '-';
        dataSave.rUserQ5 = '-';
        dataSave.rUserWinner = 'NO';
        dataSave.rCurtForm = 1;
        dataSave.rDateEnd = dateFormEnd;

        var strSave = JSON.stringify(dataSave) + '||';

        $cordovaFile.writeExistingFile(cordova.file.dataDirectory, "forms.txt", strSave)
            .then(function (success) {
                //console.log(strSave);
                $ionicLoading.hide();
                document.location.href = 'index.html';
            }, function (error) {
                $ionicLoading.hide();
                alert(error);
            });
    };

});


controllerModule.controller('informPageCtrl', function($scope,$state) {
    $scope.nextPage = function(){
        $state.go('changePage');
    };


});


controllerModule.controller('changePageCtrl', function($scope,$state,$cordovaFile,$http,$ionicLoading) {

    var rUserWinner = '';

    $scope.changeBundle = function (color) {
        rUserWinner = color;
        if(color == 'GRAY'){
            $('.change-page-i2').removeClass('active');
            $('.change-page-i3').addClass('active');
        }else{
            $('.change-page-i3').removeClass('active');
            $('.change-page-i2').addClass('active');
        }
    }

    $scope.nextPage = function(){

        if(rUserWinner != '') {
            $ionicLoading.show({
                template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Сохранение анкеты на устройство!'
            });
            var dataSave = {};
            var dateFormEnd = new Date().getTime();

            dataSave.rExCode = window.localStorage.getItem('rExCode');
            dataSave.rDateCreate = window.localStorage.getItem('rDateCreate');
            dataSave.rInCity = window.localStorage.getItem('rInCity');
            dataSave.rInUserId = window.localStorage.getItem('rInUserId');
            dataSave.rInTT = window.localStorage.getItem('rInTT');
            dataSave.rInSfdc = window.localStorage.getItem('rInSfdc');
            dataSave.rUserLat = window.localStorage.getItem('rUserLat');
            dataSave.rUserLong = window.localStorage.getItem('rUserLong');
            dataSave.rUserSign = window.localStorage.getItem('rUserSign');
            dataSave.rUserSignDate = window.localStorage.getItem('rUserSignDate');
            dataSave.rUserMarkaSig = window.localStorage.getItem('rUserMarkaSig');
            dataSave.rUserStrongSig = window.localStorage.getItem('rUserStrongSig');
            dataSave.rUserName = window.localStorage.getItem('rUserName');
            dataSave.rUserLastName = window.localStorage.getItem('rUserLastName');
            dataSave.rUserDate = window.localStorage.getItem('rUserDate');
            dataSave.rUserEmail = window.localStorage.getItem('rUserEmail');
            dataSave.rUserPhone = window.localStorage.getItem('rUserPhone');
            dataSave.rUserQ1 = window.localStorage.getItem('rUserQ1');
            dataSave.rUserQ2 = window.localStorage.getItem('rUserQ2');
            dataSave.rUserQ3 = window.localStorage.getItem('rUserQ3');
            dataSave.rUserQ4 = window.localStorage.getItem('rUserQ4');
            dataSave.rUserQ5 = '-';
            dataSave.rCurtForm = 0;
            dataSave.rUserWinner = rUserWinner;
            dataSave.rDateEnd = dateFormEnd;

            var strSave = JSON.stringify(dataSave) + '||';
            var strSaveServer = strSave.substring(0, strSave.length - 2);

            $cordovaFile.writeExistingFile(cordova.file.dataDirectory, "forms.txt", strSave)
                .then(function (success) {
                    //console.log(strSave);

                    $http.post('http://maxim.global-man.ru/local/action/api.php', {method: 'save', data: strSaveServer}, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).success(function (result) {
                        if (result.success) {
                            navigator.notification.alert(
                                'Анкета успешно сохранена на устройстве и на сервере.',
                                function(){},
                                'Внимание',
                                'Закрыть'
                            );
                        } else {
                            navigator.notification.alert(
                                'Анкета успешно сохранена на устройстве.',
                                function(){},
                                'Внимание',
                                'Закрыть'
                            );
                        }

                        document.location.href = 'index.html';
                        $ionicLoading.hide();

                    }).error(function(data, status, headers, config) {
                        console.log(data);
                        console.log(headers);
                        navigator.notification.alert(
                            'Анкета успешно сохранена на устройстве.',
                            function(){},
                            'Внимание',
                            'Закрыть'
                        );
                        document.location.href = 'index.html';
                        $ionicLoading.hide();
                    });

                }, function (error) {
                    $ionicLoading.hide();
                    alert(error);
                });
        }else {
            navigator.notification.alert(
                'Необходимо выбрать один из двух видов оплаты.',
                function(){},
                'Внимание',
                'Выбрать'
            );
            //alert('Необходимо выбрать один из двух видов оплаты.');
        }

    };

});


controllerModule.controller('lastPageCtrl', function($scope,$state,Data) {

    // Решили убрать страницу
});


function checkEmail(email){
    var re = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,6}$/i;
    return re.test(email) ? true : false;
};

function checkTextCiril(string) {
    var re = /^[а-яё,.\s]+$/i;
    if (string == '' || !re.test(string)) {
        return false;
    } else {
        return true;
    }
}

function checkTextCirilWithNumber(string) {
    var re = /^[а-яё,.\s]+$/i;
    if (string == '' || !re.test(string)) {
        return false;
    } else {
        return true;
    }
}

function getAge(dateString) {
    var day = parseInt(dateString.substring(0,2));
    var month = parseInt(dateString.substring(3,5));
    var year = parseInt(dateString.substring(6,10));

    var today = new Date();
    var birthDate = new Date(year, month, day);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}


function checkInterviewer(login) {

    var dataInterviewers = [{"id":"3","login":"abakan101"},{"id":"4","login":"abakan102"},{"id":"5","login":"abakan103"},{"id":"6","login":"abakan104"},{"id":"7","login":"abakan105"},{"id":"8","login":"abakan106"},{"id":"9","login":"abakan109"},{"id":"10","login":"abakan110"},{"id":"11","login":"abakan111"},{"id":"12","login":"abakan112"},{"id":"13","login":"abakan113"},{"id":"14","login":"abakan114"},{"id":"15","login":"abakan115"},{"id":"16","login":"abakan116"},{"id":"17","login":"abakan117"},{"id":"18","login":"abakan118"},{"id":"19","login":"abakan119"},{"id":"20","login":"abakan120"},{"id":"21","login":"barnaul201"},{"id":"22","login":"barnaul202"},{"id":"23","login":"barnaul203"},{"id":"24","login":"barnaul204"},{"id":"25","login":"barnaul205"},{"id":"26","login":"barnaul206"},{"id":"27","login":"barnaul207"},{"id":"28","login":"barnaul208"},{"id":"29","login":"barnaul209"},{"id":"30","login":"barnaul210"},{"id":"31","login":"barnaul211"},{"id":"32","login":"barnaul212"},{"id":"33","login":"barnaul213"},{"id":"34","login":"barnaul214"},{"id":"35","login":"barnaul215"},{"id":"36","login":"barnaul216"},{"id":"37","login":"barnaul217"},{"id":"38","login":"barnaul218"},{"id":"40","login":"barnaul219"},{"id":"41","login":"barnaul220"},{"id":"42","login":"biisk301"},{"id":"43","login":"biisk302"},{"id":"44","login":"biisk303"},{"id":"45","login":"biisk304"},{"id":"46","login":"biisk305"},{"id":"47","login":"biisk306"},{"id":"48","login":"biisk307"},{"id":"49","login":"biisk308"},{"id":"50","login":"biisk309"},{"id":"51","login":"biisk310"},{"id":"52","login":"biisk311"},{"id":"53","login":"biisk312"},{"id":"54","login":"biisk313"},{"id":"55","login":"biisk314"},{"id":"56","login":"biisk315"},{"id":"57","login":"biisk316"},{"id":"58","login":"biisk317"},{"id":"59","login":"biisk318"},{"id":"60","login":"biisk319"},{"id":"61","login":"biisk320"},{"id":"62","login":"blagoveshensk401"},{"id":"63","login":"blagoveshensk402"},{"id":"64","login":"blagoveshensk403"},{"id":"65","login":"blagoveshensk404"},{"id":"66","login":"blagoveshensk405"},{"id":"67","login":"blagoveshensk406"},{"id":"68","login":"blagoveshensk407"},{"id":"69","login":"blagoveshensk408"},{"id":"70","login":"blagoveshensk409"},{"id":"71","login":"blagoveshensk410"},{"id":"72","login":"blagoveshensk411"},{"id":"73","login":"blagoveshensk412"},{"id":"74","login":"blagoveshensk413"},{"id":"75","login":"blagoveshensk414"},{"id":"76","login":"blagoveshensk415"},{"id":"77","login":"blagoveshensk416"},{"id":"78","login":"blagoveshensk417"},{"id":"79","login":"blagoveshensk418"},{"id":"80","login":"blagoveshensk419"},{"id":"81","login":"blagoveshensk420"},{"id":"82","login":"bratsk501"},{"id":"83","login":"bratsk502"},{"id":"84","login":"bratsk503"},{"id":"85","login":"bratsk504"},{"id":"86","login":"bratsk505"},{"id":"87","login":"bratsk506"},{"id":"88","login":"bratsk507"},{"id":"89","login":"bratsk508"},{"id":"90","login":"bratsk509"},{"id":"91","login":"bratsk510"},{"id":"92","login":"bratsk511"},{"id":"93","login":"bratsk512"},{"id":"94","login":"bratsk513"},{"id":"95","login":"bratsk514"},{"id":"96","login":"bratsk515"},{"id":"97","login":"bratsk516"},{"id":"98","login":"bratsk517"},{"id":"99","login":"bratsk518"},{"id":"100","login":"bratsk519"},{"id":"101","login":"bratsk520"},{"id":"102","login":"vladivostok601"},{"id":"103","login":"vladivostok602"},{"id":"104","login":"vladivostok603"},{"id":"105","login":"vladivostok604"},{"id":"106","login":"vladivostok605"},{"id":"107","login":"vladivostok606"},{"id":"108","login":"vladivostok607"},{"id":"109","login":"vladivostok608"},{"id":"110","login":"vladivostok609"},{"id":"111","login":"vladivostok610"},{"id":"112","login":"vladivostok611"},{"id":"113","login":"vladivostok612"},{"id":"114","login":"vladivostok613"},{"id":"115","login":"vladivostok614"},{"id":"116","login":"vladivostok615"},{"id":"117","login":"vladivostok616"},{"id":"118","login":"vladivostok617"},{"id":"119","login":"vladivostok618"},{"id":"120","login":"vladivostok619"},{"id":"121","login":"vladivostok620"},{"id":"122","login":"ekaterinburg701"},{"id":"123","login":"ekaterinburg702"},{"id":"124","login":"ekaterinburg703"},{"id":"125","login":"ekaterinburg704"},{"id":"126","login":"ekaterinburg705"},{"id":"127","login":"ekaterinburg706"},{"id":"128","login":"ekaterinburg707"},{"id":"129","login":"ekaterinburg708"},{"id":"130","login":"ekaterinburg709"},{"id":"131","login":"ekaterinburg710"},{"id":"132","login":"ekaterinburg711"},{"id":"133","login":"ekaterinburg712"},{"id":"134","login":"ekaterinburg713"},{"id":"135","login":"ekaterinburg714"},{"id":"136","login":"ekaterinburg715"},{"id":"137","login":"ekaterinburg716"},{"id":"138","login":"ekaterinburg717"},{"id":"139","login":"ekaterinburg718"},{"id":"140","login":"ekaterinburg719"},{"id":"141","login":"ekaterinburg720"},{"id":"142","login":"irkutsk801"},{"id":"143","login":"irkutsk802"},{"id":"144","login":"irkutsk803"},{"id":"145","login":"irkutsk804"},{"id":"146","login":"irkutsk805"},{"id":"147","login":"irkutsk806"},{"id":"148","login":"irkutsk807"},{"id":"149","login":"irkutsk808"},{"id":"150","login":"irkutsk809"},{"id":"151","login":"irkutsk810"},{"id":"152","login":"irkutsk811"},{"id":"153","login":"irkutsk812"},{"id":"154","login":"irkutsk813"},{"id":"155","login":"irkutsk814"},{"id":"156","login":"irkutsk815"},{"id":"157","login":"irkutsk816"},{"id":"158","login":"irkutsk817"},{"id":"159","login":"irkutsk818"},{"id":"160","login":"irkutsk819"},{"id":"161","login":"irkutsk820"},{"id":"162","login":"kemerovo901"},{"id":"163","login":"kemerovo902"},{"id":"164","login":"kemerovo903"},{"id":"165","login":"kemerovo904"},{"id":"166","login":"kemerovo905"},{"id":"167","login":"kemerovo906"},{"id":"168","login":"kemerovo907"},{"id":"169","login":"kemerovo908"},{"id":"170","login":"kemerovo909"},{"id":"171","login":"kemerovo910"},{"id":"172","login":"kemerovo911"},{"id":"173","login":"kemerovo912"},{"id":"174","login":"kemerovo913"},{"id":"175","login":"kemerovo914"},{"id":"176","login":"kemerovo915"},{"id":"177","login":"kemerovo916"},{"id":"178","login":"kemerovo917"},{"id":"179","login":"kemerovo918"},{"id":"180","login":"kemerovo919"},{"id":"181","login":"kemerovo920"},{"id":"182","login":"komsomolsk1001"},{"id":"183","login":"komsomolsk1002"},{"id":"184","login":"komsomolsk1003"},{"id":"185","login":"komsomolsk1004"},{"id":"186","login":"komsomolsk1005"},{"id":"187","login":"komsomolsk1006"},{"id":"188","login":"komsomolsk1007"},{"id":"189","login":"komsomolsk1008"},{"id":"190","login":"komsomolsk1009"},{"id":"191","login":"komsomolsk1010"},{"id":"192","login":"komsomolsk1011"},{"id":"193","login":"komsomolsk1012"},{"id":"194","login":"komsomolsk1013"},{"id":"195","login":"komsomolsk1014"},{"id":"196","login":"komsomolsk1015"},{"id":"197","login":"komsomolsk1016"},{"id":"198","login":"komsomolsk1017"},{"id":"199","login":"komsomolsk1018"},{"id":"200","login":"komsomolsk1019"},{"id":"201","login":"komsomolsk1020"},{"id":"202","login":"magnitogorsk1101"},{"id":"203","login":"magnitogorsk1102"},{"id":"204","login":"magnitogorsk1103"},{"id":"205","login":"magnitogorsk1104"},{"id":"206","login":"magnitogorsk1105"},{"id":"207","login":"magnitogorsk1106"},{"id":"208","login":"magnitogorsk1107"},{"id":"209","login":"magnitogorsk1108"},{"id":"210","login":"magnitogorsk1109"},{"id":"211","login":"magnitogorsk1110"},{"id":"212","login":"magnitogorsk1111"},{"id":"213","login":"magnitogorsk1112"},{"id":"214","login":"magnitogorsk1113"},{"id":"215","login":"magnitogorsk1114"},{"id":"216","login":"magnitogorsk1115"},{"id":"217","login":"magnitogorsk1116"},{"id":"218","login":"magnitogorsk1117"},{"id":"219","login":"magnitogorsk1118"},{"id":"220","login":"magnitogorsk1119"},{"id":"221","login":"magnitogorsk1120"},{"id":"222","login":"nnovgorod1201"},{"id":"223","login":"nnovgorod1202"},{"id":"224","login":"nnovgorod1203"},{"id":"225","login":"nnovgorod1204"},{"id":"226","login":"nnovgorod1205"},{"id":"227","login":"nnovgorod1206"},{"id":"228","login":"nnovgorod1207"},{"id":"229","login":"nnovgorod1208"},{"id":"230","login":"nnovgorod1209"},{"id":"231","login":"nnovgorod1210"},{"id":"232","login":"nnovgorod1211"},{"id":"233","login":"nnovgorod1212"},{"id":"234","login":"nnovgorod1213"},{"id":"235","login":"nnovgorod1214"},{"id":"236","login":"nnovgorod1215"},{"id":"237","login":"nnovgorod1216"},{"id":"238","login":"nnovgorod1217"},{"id":"239","login":"nnovgorod1218"},{"id":"240","login":"nnovgorod1219"},{"id":"241","login":"nnovgorod1220"},{"id":"242","login":"nahodka1301"},{"id":"243","login":"nahodka1302"},{"id":"244","login":"nahodka1303"},{"id":"245","login":"nahodka1304"},{"id":"246","login":"nahodka1305"},{"id":"247","login":"nahodka1306"},{"id":"248","login":"nahodka1307"},{"id":"249","login":"nahodka1308"},{"id":"250","login":"nahodka1309"},{"id":"251","login":"nahodka1310"},{"id":"252","login":"nahodka1311"},{"id":"253","login":"nahodka1312"},{"id":"254","login":"nahodka1313"},{"id":"255","login":"nahodka1314"},{"id":"256","login":"nahodka1315"},{"id":"257","login":"nahodka1316"},{"id":"258","login":"nahodka1317"},{"id":"259","login":"nahodka1318"},{"id":"260","login":"nahodka1319"},{"id":"261","login":"nahodka1320"},{"id":"262","login":"novokuzneck1401"},{"id":"263","login":"novokuzneck1402"},{"id":"264","login":"novokuzneck1403"},{"id":"265","login":"novokuzneck1404"},{"id":"266","login":"novokuzneck1405"},{"id":"267","login":"novokuzneck1406"},{"id":"268","login":"novokuzneck1407"},{"id":"269","login":"novokuzneck1408"},{"id":"270","login":"novokuzneck1409"},{"id":"271","login":"novokuzneck1410"},{"id":"272","login":"novokuzneck1411"},{"id":"273","login":"novokuzneck1412"},{"id":"274","login":"novokuzneck1413"},{"id":"275","login":"novokuzneck1414"},{"id":"276","login":"novokuzneck1415"},{"id":"277","login":"novokuzneck1416"},{"id":"278","login":"novokuzneck1417"},{"id":"279","login":"novokuzneck1418"},{"id":"280","login":"novokuzneck1419"},{"id":"281","login":"novokuzneck1420"},{"id":"282","login":"novosibirsk1501"},{"id":"283","login":"novosibirsk1502"},{"id":"284","login":"novosibirsk1503"},{"id":"285","login":"novosibirsk1504"},{"id":"286","login":"novosibirsk1505"},{"id":"287","login":"novosibirsk1506"},{"id":"288","login":"novosibirsk1507"},{"id":"289","login":"novosibirsk1508"},{"id":"290","login":"novosibirsk1509"},{"id":"291","login":"novosibirsk1510"},{"id":"292","login":"novosibirsk1511"},{"id":"293","login":"novosibirsk1512"},{"id":"294","login":"novosibirsk1513"},{"id":"295","login":"novosibirsk1514"},{"id":"296","login":"novosibirsk1515"},{"id":"297","login":"novosibirsk1516"},{"id":"298","login":"novosibirsk1517"},{"id":"299","login":"novosibirsk1518"},{"id":"300","login":"novosibirsk1519"},{"id":"301","login":"novosibirsk1520"},{"id":"302","login":"perm1601"},{"id":"303","login":"perm1602"},{"id":"304","login":"perm1603"},{"id":"305","login":"perm1604"},{"id":"306","login":"perm1605"},{"id":"307","login":"perm1606"},{"id":"308","login":"perm1607"},{"id":"309","login":"perm1608"},{"id":"310","login":"perm1609"},{"id":"311","login":"perm1610"},{"id":"312","login":"perm1611"},{"id":"313","login":"perm1612"},{"id":"314","login":"perm1613"},{"id":"315","login":"perm1614"},{"id":"316","login":"perm1615"},{"id":"317","login":"perm1616"},{"id":"318","login":"perm1617"},{"id":"319","login":"perm1618"},{"id":"320","login":"perm1619"},{"id":"321","login":"perm1620"},{"id":"322","login":"samara1701"},{"id":"323","login":"samara1702"},{"id":"324","login":"samara1703"},{"id":"325","login":"samara1704"},{"id":"326","login":"samara1705"},{"id":"327","login":"samara1706"},{"id":"328","login":"samara1707"},{"id":"329","login":"samara1708"},{"id":"330","login":"samara1709"},{"id":"331","login":"samara1710"},{"id":"332","login":"samara1711"},{"id":"333","login":"samara1712"},{"id":"334","login":"samara1713"},{"id":"335","login":"samara1714"},{"id":"336","login":"samara1715"},{"id":"337","login":"samara1716"},{"id":"338","login":"samara1717"},{"id":"339","login":"samara1718"},{"id":"340","login":"samara1719"},{"id":"341","login":"samara1720"},{"id":"342","login":"ufa1801"},{"id":"343","login":"ufa1802"},{"id":"344","login":"ufa1803"},{"id":"345","login":"ufa1804"},{"id":"346","login":"ufa1805"},{"id":"347","login":"ufa1806"},{"id":"348","login":"ufa1807"},{"id":"349","login":"ufa1808"},{"id":"350","login":"ufa1809"},{"id":"351","login":"ufa1810"},{"id":"352","login":"ufa1811"},{"id":"353","login":"ufa1812"},{"id":"354","login":"ufa1813"},{"id":"355","login":"ufa1814"},{"id":"356","login":"ufa1815"},{"id":"357","login":"ufa1816"},{"id":"358","login":"ufa1817"},{"id":"359","login":"ufa1818"},{"id":"360","login":"ufa1819"},{"id":"361","login":"ufa1820"},{"id":"362","login":"habarovsk1901"},{"id":"363","login":"habarovsk1902"},{"id":"364","login":"habarovsk1903"},{"id":"365","login":"habarovsk1904"},{"id":"366","login":"habarovsk1905"},{"id":"367","login":"habarovsk1906"},{"id":"368","login":"habarovsk1907"},{"id":"369","login":"habarovsk1908"},{"id":"370","login":"habarovsk1909"},{"id":"371","login":"habarovsk1910"},{"id":"372","login":"habarovsk1911"},{"id":"373","login":"habarovsk1912"},{"id":"374","login":"habarovsk1913"},{"id":"375","login":"habarovsk1914"},{"id":"376","login":"habarovsk1915"},{"id":"377","login":"habarovsk1916"},{"id":"378","login":"habarovsk1917"},{"id":"379","login":"habarovsk1918"},{"id":"380","login":"habarovsk1919"},{"id":"381","login":"habarovsk1920"},{"id":"382","login":"chelyabinsk2001"},{"id":"383","login":"chelyabinsk2002"},{"id":"384","login":"chelyabinsk2003"},{"id":"385","login":"chelyabinsk2004"},{"id":"386","login":"chelyabinsk2005"},{"id":"387","login":"chelyabinsk2006"},{"id":"388","login":"chelyabinsk2007"},{"id":"389","login":"chelyabinsk2008"},{"id":"390","login":"chelyabinsk2009"},{"id":"391","login":"chelyabinsk2010"},{"id":"392","login":"chelyabinsk2011"},{"id":"393","login":"chelyabinsk2012"},{"id":"394","login":"chelyabinsk2013"},{"id":"395","login":"chelyabinsk2014"},{"id":"396","login":"chelyabinsk2015"},{"id":"397","login":"chelyabinsk2016"},{"id":"398","login":"chelyabinsk2017"},{"id":"399","login":"chelyabinsk2018"},{"id":"400","login":"chelyabinsk2019"},{"id":"401","login":"chelyabinsk2020"},{"id":"402","login":"abakan107"},{"id":"403","login":"abakan108"},{"id":"404","login":"test101"},{"id":"405","login":"test102"},{"id":"406","login":"test103"},{"id":"407","login":"test104"},{"id":"408","login":"test105"},{"id":"409","login":"admin101"}];
    var id = 0;

    for (var i = dataInterviewers.length - 1; i >= 0; i--) {
        if(dataInterviewers[i]['login'] == login){
            id = dataInterviewers[i]['id'];
        }
    };

    return id;
}